import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { HomeComponent } from './home/home.component';
import { ProductsComponent } from './products/products.component';
import { TechnologiesComponent } from './technologies/technologies.component';
import { MockupComponent } from './mockup/mockup.component';

const routes: Routes = [
  { path: '', component: HomeComponent },
  //{ path: 'products', component: ProductsComponent },
  { path: 'technologies', component: TechnologiesComponent },
  { path: 'mockup', component: MockupComponent },
  {
    path: '',
    redirectTo: '/home',
    pathMatch: 'full'
  },
];


@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
